package com.example.movieshowapp.controller;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.movieshowapp.R;
import com.example.movieshowapp.adapters.MoviesAdapter;
import com.example.movieshowapp.model.DBHandler;
import com.example.movieshowapp.model.MoviesGetSet;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetailMovie extends AppCompatActivity {
    ImageView imageView;
    TextView textView;
    TextView textOverview;
    Button buttonReview;
    Button buttonFavorite;

    Button buttonTrailer;

    private DBHandler dbHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);



        Intent intent = getIntent();
        String v_id_movie = intent.getStringExtra("id_movie");
        Log.e("testtt",v_id_movie);
        getDetailMovie(v_id_movie);
        buttonReview = (Button) findViewById(R.id.detail_movie_button);

        dbHandler = new DBHandler(DetailMovie.this);

        buttonReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(DetailMovie.this, Reviews.class);
//                i.putExtra("message_key", str);
                i.putExtra("id_movie", v_id_movie);
                startActivity(i);


            }
        });

        buttonFavorite = (Button) findViewById(R.id.detail_movie_button_favorite);

        buttonFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> favorites = dbHandler.readFavorite(v_id_movie);
                if(favorites.size() > 0) {
                    dbHandler.deleteFavorite(v_id_movie);
                    Toast.makeText(getApplicationContext(), "Sukses Hapus Favorit", Toast.LENGTH_LONG).show();//display the text of button1
                } else {
                    dbHandler.addNewFavorite(v_id_movie);
                    Toast.makeText(getApplicationContext(), "Sukses Tambah Favorit", Toast.LENGTH_LONG).show();//display the text of button1
                }

            }
        });

        buttonTrailer = (Button) findViewById(R.id.detail_movie_trailer);
        buttonTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(), "Simple Button 1", Toast.LENGTH_LONG).show();//display the text of button1
                getTrailerMovie(v_id_movie);

            }
        });
    }

    private void loadImage(String url) {
        Picasso.get()
                .load(url)
                .into(imageView);
    }

    private void getDetailMovie(String v_id_movie) {
        //getting the progressbar
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarMovies);
        progressBar.setVisibility(View.VISIBLE);

        String url = "https://api.themoviedb.org/3/movie/"+v_id_movie+"?language=en-US";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            String vOverview = obj.getString("overview");
                            String vPoster = obj.getString("poster_path");
                            String vOriTitle = obj.getString("original_title");
                            String vReleaseDate = obj.getString("release_date");


                            textOverview = (TextView) findViewById(R.id.detail_movie_overview);
                            textView = (TextView) findViewById(R.id.detail_movie_original_title);
                            imageView = findViewById(R.id.detail_movie_poster);


                            String poster_url = "https://image.tmdb.org/t/p/w500/"+vPoster;
                            loadImage(poster_url);

                            textOverview.setText(vOverview);
                            textView.setText(vOriTitle + " (" + (vReleaseDate).substring(0,4) + ")");
                            Log.e("reponseAPI",vOverview);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occur
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjYzE2NGY3YzkwMzc5MzQwMDc4NTVmNGU5NzFlNjI2NCIsInN1YiI6IjY0ZmYxNzdhMmRmZmQ4MDBjNjJjNDE5NiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.xMLVVC3DZJ55keq4Z_eO0jjgwDq0XbIHe6XvaBSvHCg");
                return headers;
            }
        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    private void getTrailerMovie(String v_id_movie) {
        //getting the progressbar
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarMovies);
        progressBar.setVisibility(View.VISIBLE);

        String url = "https://api.themoviedb.org/3/movie/"+v_id_movie+"/videos?language=en-US";
        Log.e("reponseAPI2",url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);
                            JSONArray trailerArray = obj.getJSONArray("results");

                            for(int i=0;i<trailerArray.length();i++)
                            {
                                JSONObject jsonObject1 = trailerArray.getJSONObject(i);
                                String vType = jsonObject1.optString("type");
                                String vSite = jsonObject1.optString("site");
                                Log.e("vSite",vSite);

                                if(vType.equals("Trailer") && vSite.equals("YouTube")){
                                    String vKeyYoutube = jsonObject1.optString("key");
                                    String vurl = "https://www.youtube.com/watch?v="+vKeyYoutube;
                                    Log.e("reponseAPI",vurl);
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(vurl));
                                    startActivity(browserIntent);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occur
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjYzE2NGY3YzkwMzc5MzQwMDc4NTVmNGU5NzFlNjI2NCIsInN1YiI6IjY0ZmYxNzdhMmRmZmQ4MDBjNjJjNDE5NiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.xMLVVC3DZJ55keq4Z_eO0jjgwDq0XbIHe6XvaBSvHCg");
                return headers;
            }
        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }
}