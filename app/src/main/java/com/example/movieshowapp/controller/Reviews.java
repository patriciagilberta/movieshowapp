package com.example.movieshowapp.controller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.movieshowapp.R;
import com.example.movieshowapp.adapters.ReviewsAdapter;
import com.example.movieshowapp.model.ReviewsGetSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Reviews extends AppCompatActivity {
    TextView receiver_msg;
    ListView listView;
    List<ReviewsGetSet> reviewList;

    public int cntPage = 1;
    public boolean loadData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);

        Intent intent = getIntent();
        String str = intent.getStringExtra("message_key");
        String v_id_movie = intent.getStringExtra("id_movie");


        listView = (ListView) findViewById(R.id.listViewReviews);
        reviewList = new ArrayList<ReviewsGetSet>();
        getListReviews(v_id_movie, cntPage);

        listView = (ListView) findViewById(R.id.listViewReviews);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ReviewsGetSet reviewsGetSet = (ReviewsGetSet) parent.getItemAtPosition(position);

                String idmovie = reviewsGetSet.getReviewAuthor(); // this is hidden id
                Log.e("test",idmovie+"lalala");

//                Intent i = new Intent(Movies.this, DetailMovie.class);
//                startActivity(i);
//
//                //String selectedItem = (String) parent.getItemAtPosition(position);
////                textView.setText("The best football player is : " + selectedItem);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                Log.e("scrolllll",cntPage +"test");
                if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0)
                {
                    if(loadData == false)
                    {
                        loadData = true;
                        getListReviews(v_id_movie, cntPage++);
                    }
                }
            }
        });

    }

    private void getListReviews(String v_id_movie, int pagenum) {
        //getting the progressbar
        loadData = true;
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarMovies);
        progressBar.setVisibility(View.VISIBLE);

        String url = "https://api.themoviedb.org/3/movie/"+v_id_movie+"/reviews?language=en-US&page="+pagenum;
        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            //we have the array named tutorial inside the object
                            //so here we are getting that json array
                            JSONArray moviessArray = obj.getJSONArray("results");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < moviessArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject moviesGetSetObject = moviessArray.getJSONObject(i);

                                //creating a tutorial object and giving them the values from json object
                                ReviewsGetSet reviewsGetSet = new ReviewsGetSet(moviesGetSetObject.getString("author"), moviesGetSetObject.getString("content"));
                                reviewList.add(reviewsGetSet);
                            }

                            //creating custom adapter object
                            ReviewsAdapter adapter = new ReviewsAdapter(reviewList, getApplicationContext());

                            //adding the adapter to listview
                            listView.setAdapter(adapter);
                            loadData = false;

                            if (moviessArray.length() == 0 && reviewList.size() == 0) {
                                Log.e("cek","gaada review");
                                receiver_msg = findViewById(R.id.receiver_msg);
                                receiver_msg.setText("Belum ada review");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            loadData = false;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occur
                        loadData = false;
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjYzE2NGY3YzkwMzc5MzQwMDc4NTVmNGU5NzFlNjI2NCIsInN1YiI6IjY0ZmYxNzdhMmRmZmQ4MDBjNjJjNDE5NiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.xMLVVC3DZJ55keq4Z_eO0jjgwDq0XbIHe6XvaBSvHCg");
                return headers;
            }
        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }
}