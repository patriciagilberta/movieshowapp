package com.example.movieshowapp.controller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.movieshowapp.MainActivity;
import com.example.movieshowapp.R;
import com.example.movieshowapp.Tutorial;
import com.example.movieshowapp.adapters.MoviesAdapter;
import com.example.movieshowapp.model.MoviesGetSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Movies extends AppCompatActivity {
    TextView receiver_msg;
    ListView listView;
    public int cntPage = 1;
    public boolean loadData = false;
    List<MoviesGetSet> moviesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);

        Intent intent = getIntent();
        String str = intent.getStringExtra("message_key");
        String v_id_movie = intent.getStringExtra("id_movie");

        listView = (ListView) findViewById(R.id.listViewMovies);
        moviesList = new ArrayList<MoviesGetSet>();
        getListMovies(v_id_movie, cntPage);

        listView = (ListView) findViewById(R.id.listViewMovies);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MoviesGetSet moviesGetSet = (MoviesGetSet) parent.getItemAtPosition(position);

                String idmovie = moviesGetSet.getIdMovie(); // this is hidden id
                String original_title = moviesGetSet.getOriginal_title();
                Log.e("test",idmovie+"lalala");
//
                Intent i = new Intent(Movies.this, DetailMovie.class);
                i.putExtra("message_key", str);
                i.putExtra("id_movie", idmovie);
                i.putExtra("original_title", original_title);
                startActivity(i);
//
//                //String selectedItem = (String) parent.getItemAtPosition(position);
////                textView.setText("The best football player is : " + selectedItem);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                Log.e("scrolllll",cntPage +"test");

                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && !(loadData)) {
                    // load more
                    getListMovies(v_id_movie, cntPage++);
                }

//                if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0)
//                {
//                    if(loadData == false)
//                    {
//                        loadData = true;
//                        getListMovies(v_id_movie, cntPage++);
//                    }
//                }
            }
        });
    }

    private void getListMovies(String v_id_movie, int cntPage) {
        loadData = true;
        //getting the progressbar
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarMovies);
        progressBar.setVisibility(View.VISIBLE);

        String JSON_URL = "https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=en-US&page="+cntPage+"&sort_by=popularity.desc&with_genres=";
        String url = JSON_URL + v_id_movie;
        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            //we have the array named tutorial inside the object
                            //so here we are getting that json array
                            JSONArray moviessArray = obj.getJSONArray("results");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < moviessArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject moviesGetSetObject = moviessArray.getJSONObject(i);

                                //creating a tutorial object and giving them the values from json object
                                MoviesGetSet moviesGetSet = new MoviesGetSet(moviesGetSetObject.getString("original_title"),moviesGetSetObject.getString("id"));
                                moviesList.add(moviesGetSet);
                            }

                            //creating custom adapter object
                            MoviesAdapter adapter = new MoviesAdapter(moviesList, getApplicationContext());

                            //adding the adapter to listview
                            listView.setAdapter(adapter);
                            loadData = false;

                            if (moviessArray.length() == 0 && moviesList.size() == 0) {
                                Log.e("cek","gaada genre");
                                Toast.makeText(getApplicationContext(), "Tidak ada data movie", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            loadData = false;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadData = false;
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjYzE2NGY3YzkwMzc5MzQwMDc4NTVmNGU5NzFlNjI2NCIsInN1YiI6IjY0ZmYxNzdhMmRmZmQ4MDBjNjJjNDE5NiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.xMLVVC3DZJ55keq4Z_eO0jjgwDq0XbIHe6XvaBSvHCg");
                return headers;
            }
        };

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }
}