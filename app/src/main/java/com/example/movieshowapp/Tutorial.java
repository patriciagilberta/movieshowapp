package com.example.movieshowapp;

public class Tutorial {
    String name,  id_movie;

    public Tutorial(String name, String id_movie) {
        this.name = name;
        this.id_movie = id_movie;
    }
    public String getName() {
        return name;
    }
    public String getIdmovie() {
        return id_movie;
    }
}