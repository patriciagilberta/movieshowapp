package com.example.movieshowapp.model;

public class MoviesGetSet {
    String original_title;
    String id_moview;

    public MoviesGetSet(String original_title, String id_moview) {

        this.original_title = original_title;
        this.id_moview = id_moview;
    }

    public String getOriginal_title() {
        return original_title;
    }
    public String getIdMovie() {
        return id_moview;
    }
}
