package com.example.movieshowapp.model;

public class ReviewsGetSet {
    String author;
    String content_review;

    public ReviewsGetSet(String author, String content_review) {

        this.author = author;
        this.content_review = content_review;
    }

    public String getReviewAuthor() {
        return author;
    }
    public String getReviewContent() {
        return content_review;
    }
}
