package com.example.movieshowapp.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.movieshowapp.R;
import com.example.movieshowapp.model.ReviewsGetSet;

import java.util.List;

public class ReviewsAdapter extends ArrayAdapter<ReviewsGetSet>  {
    private List<ReviewsGetSet> reviewsList;
    private Bitmap bitmap;
    private Context mCtx;
    public ReviewsAdapter(List<ReviewsGetSet> reviewsList, Context mCtx) {
        super(mCtx, R.layout.list_item_reviews, reviewsList);
        this.reviewsList = reviewsList;
        this.mCtx = mCtx;
    }

    //this method will return the list item
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //getting the layoutinflater
        ReviewsAdapter.ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        convertView = inflater.inflate(R.layout.list_item_reviews, null, true);
        holder = new ReviewsAdapter.ViewHolder();
        holder.text_review_author = convertView.findViewById(R.id.text_review_author);
        holder.text_review = convertView.findViewById(R.id.text_review);

        convertView.setTag(holder);
        ReviewsGetSet reviewsGetSet = reviewsList.get(position);
        String tutorialTitle = reviewsGetSet.getReviewAuthor();
        String vContent = reviewsGetSet.getReviewContent();

        holder.text_review_author.setText(tutorialTitle);
        holder.text_review.setText(vContent);

        return convertView;
    }
    static class ViewHolder {
        TextView text_review_author;
        TextView text_review;
    }

}
