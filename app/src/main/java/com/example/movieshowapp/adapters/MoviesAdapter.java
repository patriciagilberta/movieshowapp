package com.example.movieshowapp.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.movieshowapp.R;
import com.example.movieshowapp.model.MoviesGetSet;
import com.example.movieshowapp.model.ReviewsGetSet;
import java.util.List;

public class MoviesAdapter extends ArrayAdapter<MoviesGetSet>  {
    private List<MoviesGetSet> moviesList;
    private Bitmap bitmap;
    private Context mCtx;
    public MoviesAdapter(List<MoviesGetSet> moviesList, Context mCtx) {
        super(mCtx, R.layout.list_item_movies, moviesList);
        this.moviesList = moviesList;
        this.mCtx = mCtx;
    }

    //this method will return the list item
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //getting the layoutinflater
        MoviesAdapter.ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        convertView = inflater.inflate(R.layout.list_item_movies, null, true);
        holder = new MoviesAdapter.ViewHolder();
        holder.text_original_title = convertView.findViewById(R.id.text_original_title);

        convertView.setTag(holder);
        MoviesGetSet moviesGetSet = moviesList.get(position);
        String tutorialTitle = moviesGetSet.getOriginal_title();

        holder.text_original_title.setText(tutorialTitle);

        return convertView;
    }
    static class ViewHolder {
        TextView text_original_title;
//        TextView textDescription;
    }

}
